# Readme
## What is Hashar?
"Hashar" is a file integrity concept based on cryptographic hashes.  For example, sha1 or sha256 algorithms are used to verify a file's integrity.
## Hashar Deep
Hashar Deep is a Ruby-based implementation of the "Hashar" concept utilizing the native [md5deep](http://md5deep.sourceforge.net/) command-line utility.  Currently, this implementation only supports *NIX-based OS's.
## Dependencies
* Ruby 1.9.3 or greater
* bundler gem to obtain required gems
* [md5deep](http://md5deep.sourceforge.net/)
* *NIX-based OS (Linux, OSX, etc)

