#
# verifydeep_spec.rb
# https://bitbucket.org/torresj/hashar-deep
# Licensed under the terms of the MIT License, as specified below.
#
# Copyright (c) 2012 Jeremy Torres, https://bitbucket.org/torresj/hashar-deep
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

require "rspec"
require 'openssl'
require_relative "../verify_deep"

describe VerifyDeep do
  describe ".algos" do
    it "returns a non-null instance" do
      VerifyDeep.algos.should_not be_nil
    end
    it "returns algorithm-to-executable hashes" do
      VerifyDeep.algos.keys.should satisfy do |keys|
        (keys - [:sha1, :sha256]).empty?
      end
    end
  end
  describe ".hash_dir" do
    context "when invalid directory" do
      it "raises invalid directory error" do
        expect {
          VerifyDeep.hash_dir("invalid_dir", nil, nil) }.to raise_error
      end
    end
    context "when valid directory" do
      before(:all) do
        @directory = File.join(File.dirname(__FILE__), "test_files")
        @test_files = Dir.glob(File.join(@directory, "*.out"))
      end
      it "generates successful sha1 hash" do
        @test_files.each do |file|
          #puts "File: #{file}"
          sha1 = OpenSSL::Digest::SHA1.hexdigest(File.read(file))
          #puts "Open SSL sha1 #{sha1}"
          hash = VerifyDeep.hash_dir(@directory, "*.out", :sha1, false)
          #puts hash
          hash[sha1].should eq file
        end
      end
      it "generates successful sha256 hash" do
        @test_files.each do |file|
          #puts "File: #{file}"
          sha1 = OpenSSL::Digest::SHA256.hexdigest(File.read(file))
          #puts "Open SSL sha1 #{sha1}"
          hash = VerifyDeep.hash_dir(@directory, "*.out", :sha256, false)
          #puts hash
          hash[sha1].should eq file
        end
      end
    end
  end
end
