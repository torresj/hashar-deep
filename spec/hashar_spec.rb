#
# hashar_spec.rb
# https://bitbucket.org/torresj/hashar-deep
# Licensed under the terms of the MIT License, as specified below.
#
# Copyright (c) 2012 Jeremy Torres, https://bitbucket.org/torresj/hashar-deep
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

require "rspec"
require 'rake'
require_relative "../verify_deep"

describe Hashar do
  context "when invalid database" do
    before :all do
      load File.join(File.dirname(__FILE__), "..", "Rakefile")
      Rake::Task[:rm_db].execute
    end
    it "raises an error" do
      expect { Hashar.new }.to_not raise_error
    end
  end
  describe ".main" do
    # get command line args
    # dir, glob_str, algo, show_progress = ARGV[0], ARGV[1], ARGV[2], ARGV[3]

    context "when incorrect number of parameters specified" do
      it "raises an error without parameters" do
        expect { Hashar.main }.to raise_error
      end
    end
  end

  ### invoking hashar as a script ###

  context "when attempting to pass parameters as script" do
    before :all do
      # reinitialize database
      load File.join(File.dirname(__FILE__), "..", "Rakefile")
      Rake::Task[:rm_db].execute
      Rake::Task[:migrate].execute
      @directory = File.join(File.dirname(__FILE__), "test_files")
    end
    it "returns error code with no parameters" do
      `ruby #{File.join(File.dirname(__FILE__), "..", "verify_deep.rb")}`
      exit_status = $?.exitstatus
      exit_status.should eq 1
    end
    it "returns error code with invalid directory parameter" do
      `ruby #{File.join(File.dirname(__FILE__), "..", \
           "verify_deep.rb "" *.txt sha1")}`
      exit_status = $?.exitstatus
      exit_status.should eq 1
    end
    it "returns error code with invalid glob pattern syntax" do
      `ruby #{File.join(File.dirname(__FILE__), "..", \
           "verify_deep.rb #{@directory} '' sha1")}`
      exit_status = $?.exitstatus
      exit_status.should eq 1
    end
    it "returns error code with invalid algorithm" do
      `ruby #{File.join(File.dirname(__FILE__), "..", \
           "verify_deep.rb #{@directory} *.txt sha")}`
      exit_status = $?.exitstatus
      exit_status.should eq 1
    end
    it "returns success code with a non-matching (valid) glob pattern" do
      `ruby #{File.join(File.dirname(__FILE__), "..", \
           "verify_deep.rb #{@directory} *.txt sha1")}`
      exit_status = $?.exitstatus
      exit_status.should eq 0
    end
  end
end
