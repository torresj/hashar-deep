#
# Rakefile
# https://bitbucket.org/torresj/hashar-deep
# Licensed under the terms of the MIT License, as specified below.
#
# Copyright (c) 2012 Jeremy Torres, https://bitbucket.org/torresj/hashar-deep
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
require 'rake/clean'
require 'active_record'
require 'yaml'
require 'logger'
require 'yard'
require 'rspec/core/rake_task'

task :default => :migrate

directory "doc"

desc "Migrate the database through scripts in db/migrate. Target specific " \
     "version with VERSION=x"
task :migrate => :environment do
  ActiveRecord::Migrator.migrate('db/migrate', ENV["VERSION"] ?
      ENV["VERSION"].to_i : nil)
end

task :environment do
  ActiveRecord::Base.establish_connection(
      YAML::load(File.open('db/database.yml'))[ENV['ENV'] ?
                                                   ENV['ENV'] : 'development'])
  ActiveRecord::Base.logger = Logger.new(File.open('db/database.log', 'a'))
end

desc "Deletes the database and log create by activerecord"
task :rm_db do
  [File.join(".", "db/hashar_db.sqlite3"),
   File.join(".", "db/database.log")].each do |f|
    File.delete(f) if File.exists?(f)
  end
end

# documentation namespace
namespace :documentation do
  desc "Generate Yardoc docs"
  YARD::Rake::YardocTask.new do |t|
    t.files = %w(**/*.rb)
    #t.options = %w(--one-file)
  end
end

desc "Documentation generation task"
task "rdoc" => %w(doc documentation:yard)

# test spec
RSpec::Core::RakeTask.new(:spec) do |t|
  t.rspec_opts = ["--color", "--format documentation"]
  #t.pattern = 'spec/*_spec.rb'
end

CLOBBER.include('db/database.log', 'db/hashar_db.sqlite3', 'doc')
