require 'mongo_mapper'

class Entity
  include MongoMapper::Document

  key :file_name, String
  key :file_hash, String
  key :processed_at, Time

  timestamps!
end